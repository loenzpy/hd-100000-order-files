
   {if $sd_all_orders_files}

       <table width="100%" class="table table-middle">
        <thead>
        <tr>
            <th  class="left">
        &nbsp;
            </th>
        </tr>

        {foreach from=$sd_all_orders_files item="attachment" key="id"}

            <tr>
                <td>
                        <div class="text-type-value">
                        {if $attachment.filename}
                            &#9643; {__("order")} # {$attachment.object_id} &#8212;
                            <a href="{"sd_order_files.getfile?attachment_id=`$attachment.attachment_id`&object_type=order&object_id=`$attachment.object_id`"|fn_url}">{$attachment.description}  &#8212; ( {$attachment.filename} )</a> ({$attachment.filesize|formatfilesize nofilter})
                        {/if}
                        </div>                    
                </td>
            </tr>


        {/foreach}
        </table>
{else}        
    <p class="ty-no-items">{__("no_items")}</p>
{/if}

{capture name="mainbox_title"}{__("files_in_orders")}{/capture}