{if !$active_tab}
    {assign var="active_tab" value=$smarty.request.selected_section}
{/if}

{if $selected_section == "files"}

{assign var="page_title" value=__("order_file")}

   <table width="100%" class="table table-middle">
    <thead>
    <tr>
        <th  class="left">
    {__("files_in_orders")}
        </th>
    </tr>
    </thead>

    {foreach from=$sd_all_orders_files item="attachment" key="id"}

    {if $attachment.object_id == $smarty.request.order_id}
        <tr>
            <td>
                {if $attachment.filename}
                    <div class="text-type-value">
                        <a href="{"sd_order_files.getfile?attachment_id=`$attachment.attachment_id`&object_type=order&object_id=`$attachment.object_id`"|fn_url}">{$attachment.description} ( {$attachment.filename} )</a> ({$attachment.filesize|formatfilesize nofilter})
                    </div>
                {/if}
            </td>
        </tr>
    {/if}

    {/foreach}
    </table>

{/if}