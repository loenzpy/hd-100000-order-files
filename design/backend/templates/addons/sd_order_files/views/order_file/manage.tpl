{assign var="page_title" value=__("order_file")}

{capture name="mainbox"}

	<table width="100%" class="table table-middle">
	<thead>
	<tr>
	    <th  class="left">
	{__("orders")}
	    </th>
	    <th  class="left">
	{__("files")}
	    </th>
	</tr>
	</thead>
	{foreach from=$sd_all_orders_files item="attachment" key="id"}
	<tr>
	    <td>
			<a href="{"orders.details?order_id=`$attachment.object_id`"|fn_url}" class="underlined">{__("order")}
			#{$attachment.object_id}</a>
	    </td>
	    <td>
	        {if $attachment.filename}
	            <div class="text-type-value">
	                <a href="{"sd_order_files.getfile?attachment_id=`$attachment.attachment_id`&object_type=order&object_id=`$attachment.object_id`"|fn_url}">{$attachment.description} ( {$attachment.filename} )</a> ({$attachment.filesize|formatfilesize nofilter})
	            </div>
	        {/if}
	    </td>
	</tr>
	{/foreach}
	</table>

	{include file="common/pagination.tpl" div_id=$smarty.request.content_id}

	{capture name="adv_buttons"} 
	    {hook name="orders:manage_tools"}
	        {include file="common/tools.tpl" tool_href="order_management.new" prefix="bottom" hide_tools="true" title=__("add_order") icon="icon-plus"}
	    {/hook}
	{/capture}

{/capture}

{include file="common/mainbox.tpl" title=$page_title sidebar=$smarty.capture.sidebar content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons content_id="manage_orders"}
