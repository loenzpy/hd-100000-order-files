<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Storage;
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_get_sd_order_files($object_type, $object_id, $type = 'M', $lang_code = CART_LANGUAGE)
{

    $condition = '';

    if (AREA != 'A') {
        $auth = Tygh::$app['session']['auth'];
        $condition = ' AND (' . fn_find_array_in_set($auth['usergroup_ids'], 'usergroup_ids', true) . ") AND status = 'A'";
    }


    return db_get_array("SELECT ?:sd_order_files.*, ?:sd_order_files_descriptions.description FROM ?:sd_order_files LEFT JOIN ?:sd_order_files_descriptions ON ?:sd_order_files.attachment_id = ?:sd_order_files_descriptions.attachment_id AND lang_code = ?s WHERE object_type = ?s AND object_id = ?i AND type = ?s ?p ORDER BY position", $lang_code, $object_type, $object_id, $type, $condition);

}

function fn_get_sd_all_orders_files()
{

    return db_get_array("SELECT DISTINCT ?:sd_order_files.*, ?:sd_order_files_descriptions.description FROM ?:sd_order_files LEFT JOIN ?:sd_order_files_descriptions ON ?:sd_order_files.attachment_id = ?:sd_order_files_descriptions.attachment_id ORDER BY position", 'order');

}

function fn_update_sd_order_files($attachment_data, $attachment_id, $object_type, $object_id, $type = 'M', $files = null, $lang_code = DESCR_SL)
{
    $object_id = intval($object_id);
    $directory = $object_type . '/' . $object_id;

    if ($files != null) {
        $uploaded_data = $files;
    } else {
        $uploaded_data = fn_filter_uploaded_data('attachment_files');
    }

    if (!empty($attachment_id)) {

        $rec = array (
            'usergroup_ids' => empty($attachment_data['usergroup_ids']) ? '0' : implode(',', $attachment_data['usergroup_ids']),
            'position' => $attachment_data['position']
        );

        db_query("UPDATE ?:sd_order_files_descriptions SET description = ?s WHERE attachment_id = ?i AND lang_code = ?s", $attachment_data['description'], $attachment_id, $lang_code);
        db_query("UPDATE ?:sd_order_files SET ?u WHERE attachment_id = ?i AND object_type = ?s AND object_id = ?i AND type = ?s", $rec, $attachment_id, $object_type, $object_id, $type);

        fn_set_hook('sd_order_files_update_file', $attachment_data, $attachment_id, $object_type, $object_id, $type, $files, $lang_code, $uploaded_data);
    } elseif (!empty($uploaded_data)) {
        $rec = array (
            'object_type' => $object_type,
            'object_id' => $object_id,
            'usergroup_ids' => empty($attachment_data['usergroup_ids']) ? '0' : implode(',', $attachment_data['usergroup_ids']),
            'position' => $attachment_data['position']
        );

        if ($type !== null) {
            $rec['type'] = $type;
        } elseif (!empty($attachment_data['type'])) {
            $rec['type'] = $attachment_data['type'];
        }

        $attachment_id = db_query("INSERT INTO ?:sd_order_files ?e", $rec);

        if ($attachment_id) {
            // Add file description
            foreach (fn_get_translation_languages() as $lang_code => $v) {
                $rec = array (
                    'attachment_id' => $attachment_id,
                    'lang_code' => $lang_code,
                    'description' => is_array($attachment_data['description']) ? $attachment_data['description'][$lang_code] : $attachment_data['description']
                );

                db_query("INSERT INTO ?:sd_order_files_descriptions ?e", $rec);
            }

            $uploaded_data[$attachment_id] = $uploaded_data[0];
            unset($uploaded_data[0]);
        }

        fn_set_hook('sd_order_files_add_file', $attachment_data, $object_type, $object_id, $type, $files, $attachment_id, $uploaded_data);
    }

    if ($attachment_id && !empty($uploaded_data[$attachment_id]) && $uploaded_data[$attachment_id]['size']) {
        $filename = $uploaded_data[$attachment_id]['name'];
        $old_filename = db_get_field("SELECT filename FROM ?:sd_order_files WHERE attachment_id = ?i", $attachment_id);

        if ($old_filename) {
            Storage::instance('files')->delete($directory . '/' . $old_filename);
        }

        list($filesize, $filename) = Storage::instance('files')->put($directory . '/' . $filename, array(
            'file' => $uploaded_data[$attachment_id]['path']
        ));

        if ($filesize) {
            $filename = fn_basename($filename);
            db_query("UPDATE ?:sd_order_files SET filename = ?s, filesize = ?i WHERE attachment_id = ?i", $filename, $filesize, $attachment_id);
        }
    }

    return $attachment_id;
}

function fn_delete_sd_order_files($attachment_ids, $object_type, $object_id)
{
    fn_set_hook('sd_order_files_delete_file', $attachment_ids, $object_type, $object_id);

    $data = db_get_array("SELECT * FROM ?:sd_order_files WHERE attachment_id IN (?n) AND object_type = ?s AND object_id = ?i", $attachment_ids, $object_type, $object_id);

    foreach ($data as $entry) {
        Storage::instance('files')->delete($entry['object_type'] . '/' . $object_id . '/' . $entry['filename']);
    }

    db_query("DELETE FROM ?:sd_order_files WHERE attachment_id IN (?n) AND object_type = ?s AND object_id = ?i", $attachment_ids, $object_type, $object_id);
    db_query("DELETE FROM ?:sd_order_files_descriptions WHERE attachment_id IN (?n)", $attachment_ids);

    return true;
}

/**
 * Get file attachment and send it to the output stream
 *
 * @param  int $attachment_id ID of attachment file
 *
 * @return boolean false if attachment could not be obtained
 */
function fn_get_sd_order_file($attachment_id)
{
    $auth = Tygh::$app['session']['auth'];

    $condition = '';
    if (AREA != 'A') {
        $condition = ' AND (' . fn_find_array_in_set($auth['usergroup_ids'], 'usergroup_ids', true) . ") AND status = 'A'";
    }

    $data = db_get_row("SELECT * FROM ?:sd_order_files WHERE attachment_id = ?i ?p", $attachment_id, $condition);

    fn_set_hook('sd_order_files_get_attachment', $data, $attachment_id);


    if (empty($data)) {
        return false;
    }

    $attachment_obj = Storage::instance('files');
    $attachment_filename = $data['object_type'] . '/' . $data['object_id'] . '/' . $data['filename'];

    if (!$attachment_obj->isExist($attachment_filename)) {
        return false;
    }

    $attachment_obj->get($attachment_filename);

    exit;
}

/**
 * Function clone product's attachments
 *
 * @param int $product_id old product id
 * @param int $pid new product id
 */
function fn_sd_order_files_clone_order(&$order_id, &$pid)
{
    $add_data = array();
    $sd_order_files = db_get_array("SELECT * FROM ?:sd_order_files WHERE object_type = 'order' AND object_id = ?i", $order_id);

    foreach ($sd_order_files as &$sd_order_files) {
        $sd_order_files_descriptions = db_get_array("SELECT * FROM ?:sd_order_files_descriptions WHERE attachment_id = ?i", $sd_order_files['attachment_id']);

        $sd_order_files['attachment_id'] = 0;
        $sd_order_files['object_id'] = $pid;

        $attachment_id = db_query("INSERT INTO ?:sd_order_files ?e", $sd_order_files);

        Storage::instance('files')->copy('orders/' . $order_id, 'order/' . $pid);

        foreach ($sd_order_files_descriptions as $descr) {
            $descr['attachment_id'] = $attachment_id;
            db_query("INSERT INTO ?:sd_order_files_descriptions ?e", $descr);
        }
    }
}

/**
 * Function delete order's attachments
 *
 * @param int $product_id product id
 */
function fn_sd_order_files_delete_order_post(&$order_id)
{
    $sd_order_files = db_get_fields("SELECT attachment_id FROM ?:sd_order_files WHERE object_type = 'order' AND object_id = ?i", $order_id);

    Storage::instance('files')->deleteDir('order/' . $order_id);

    foreach ($sd_order_files as $sd_order_files_id) {
        db_query("DELETE FROM ?:sd_order_files WHERE attachment_id = ?i", $attachment_id);
        db_query("DELETE FROM ?:sd_order_files_descriptions WHERE attachment_id = ?i", $attachment_id);
    }
}

/**
 * Checks permission to work with the attachment
 *
 * @param array $request Array of query parameters
 * @return bool Permission to work with attachment
 */
function fn_sd_order_files_check_permission($request)
{
    /**
     * Changes input parameters for attachment permission check
     *
     * @param array $request Array of query parameters
     */
    fn_set_hook('sd_order_files_check_permission_pre', $request);

    $permission = false;

    if (!empty($request['object_type']) && !empty($request['object_id'])) {
        $table = "orders";
        $field = "order_id";
        $condition = "AND {$field} = {$request['object_id']} " . fn_get_company_condition("?:{$table}.company_id");

        /**
         * Checks permission to work with the attachment
         *
         * @param array $request Array of query parameters
         * @param string $table Table to perform check
         * @param string $field SQL field to be selected in an SQL-query
         * @param string $condition String containing SQL-query condition prepended with a logical operator (AND or OR)
         */
        fn_set_hook('sd_order_files_check_permission', $request, $table, $field, $condition);
        $object_id = db_get_field(
            "SELECT ?f FROM ?:?f WHERE 1 ?p",
            $field,
            $table,
            $condition
        );
        if (!empty($object_id)) {
            $permission = true;
        }
    }

    /**
     * Changes result of attachment permission check
     *
     * @param array $request Array of query parameters
     */
    fn_set_hook('sd_order_files_check_permission_post', $request, $permission);
    return $permission;
}

/**
* Connect to HOOK get_orders.
* Add the parameters of the query field and the conditions.
*
* @param array of HOOK from get_orders
* @return bool array $params order files
* @return array fields adding a new field to the request get orders
* @return string condition adding string in SQL request
* @return string join adding string in SQL request
**/

function fn_sd_order_files_get_orders (&$params, &$fields, &$sortings, &$condition, &$join, &$group) {

    if (isset($_REQUEST['files_in_orders'])) {

            $params['order_files'] = true;
    }

    if (!empty($params['order_files'])) {

        $fields[] = '?:sd_order_files.object_id as order_files';

        $condition .= db_quote(" AND ?:sd_order_files.object_id > ?i", 0);

        $join .= " LEFT JOIN ?:sd_order_files ON ?:sd_order_files.object_id = ?:orders.order_id";
    }

}

/**
* Connect to HOOK mailer_send_pre from function send Tygh-class Mailer
* Allows you to add attachment files to a letter
* 
*
**/

/*
function fn_sd_order_files_mailer_send_pre($mailer, $transport, $message, $area, $lang_code)
{

    if (strpos($message->getId(), 'order_notification') === 0) {
        $data = $message->getData();
        $params = $message->getParams();

        $file_name = db_get_field("SELECT filename FROM ?:sd_order_files WHERE object_id = ?i", $data['order_info']['order_id']);

        if (!empty($params['attach_order_document']) && !empty($data['order_info']['order_id'])) {
            $invoice_path = Registry::get('config.dir.files') . 'order/' . $data['order_info']['order_id'] . $file_name;

            //$message->addAttachment($invoice_path, basename($invoice_path));

           // $message->addAttachment('/var/www/html/dpirmatov/cscart/var/files/order/119/GIT.txt', 'GIT.txt');

// Запрос получения списка файлов заказа
            $sd_order_files = fn_get_sd_order_files('order',  $data['order_info']['order_id'], 'M', DESCR_SL);
// Запись строк HTML ссылок  на файлы

            // index.php?dispatch=sd_order_files.getfile&attachment_id=3&object_type=order&object_id=103
            
            //$textmy = 'index.php?dispatch=sd_order_files.getfile&attachment_id=' . $sd_order_files[0]['attachment_id'] . '&object_type=' . $sd_order_files[0]['object_type'] . '&object_id=' . $sd_order_files[0]['object_id'];
            //fn_print_die($mailer);
            
            register_shutdown_function(function() use($invoice_path){
                fn_rm($invoice_path);
            });
        }
    }

    
}
*/

/**
* Connect to HOOK checkout_select_default_payment_method
*
* @param int order_id in REQUEST
* @return array sd_order_files for smarty template
*
**/

function fn_sd_order_files_checkout_select_default_payment_method($cart, $payment_methods, $completed_steps)
{

    if (!empty($_REQUEST['order_id'])) {
        $sd_order_files = fn_get_sd_order_files('order', $_REQUEST['order_id'], 'M', DESCR_SL);

        Tygh::$app['view']->assign('sd_order_files', $sd_order_files);    
    }
    

}


/**
* Connect to HOOK send_order_notification in fn_order_notification in the file fn.cart.php
* Create a new array element with an HTML string to output to the notification letter template
*
* @param  array $order_info by order ID
* @return array order_info files
*
**/

function fn_sd_order_files_send_order_notification(&$order_info, $edp_data, $force_notification, $notified, $send_order_notification) {

    $sd_order_files = fn_get_sd_order_files('order', $order_info['order_id'], 'M', DESCR_SL);

    if (!empty($sd_order_files)) {

        $link_order_files = '<b>Прикрепленные файлы:</b><br>';
        foreach ($sd_order_files as $key => $value) {
            $link_order_files = $link_order_files . "<a href=http://localhost/dpirmatov/cscart/index.php?dispatch=sd_order_files.getfile&attachment_id=" . $sd_order_files[$key]['attachment_id'] . "&object_type=" . $sd_order_files[$key]['object_type'] . "&object_id=" . $sd_order_files[$key]['object_id'] ."> " . $sd_order_files[$key]['description'] . " </a><br>";
        }

        $order_info['files'] = $link_order_files;
        
    }

}
