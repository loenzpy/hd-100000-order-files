<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
	'get_orders',
    'clone_order',
    'delete_order_post',
   // 'mailer_send_pre',   <<-- For attaching files to letters
    'checkout_select_default_payment_method',
    'send_order_notification'
);

Registry::set('config.storage.files', array(
    'prefix' => 'files',
    'secured' => true,
    'dir' => Registry::get('config.dir.var')
));
