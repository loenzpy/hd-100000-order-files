<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if (Registry::get('runtime.company_id')) {
    $permission = fn_sd_order_files_check_permission($_REQUEST);
    if (!$permission) {
        fn_set_notification('W', __('warning'), __('access_denied'));
        if (defined('AJAX_REQUEST')) {
            exit;
        } else {
            return array(CONTROLLER_STATUS_DENIED);
        }
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    //
    // Create/update attachments
    //
    if ($mode == 'update') {

        if (!empty($_REQUEST['attachment_data'])) {
            fn_update_sd_order_files($_REQUEST['attachment_data'], $_REQUEST['attachment_id'], $_REQUEST['object_type'], $_REQUEST['object_id'], 'M', null, DESCR_SL);

            
        }

    }

    if ($mode == 'delete') {
        fn_delete_sd_order_files(array($_REQUEST['attachment_id']), $_REQUEST['object_type'], $_REQUEST['object_id']);
        $attachments = fn_get_sd_order_files($_REQUEST['object_type'], $_REQUEST['object_id']);
        if (empty($attachments)) {
            Tygh::$app['view']->assign('object_type', $_REQUEST['object_type']);
            Tygh::$app['view']->assign('object_id', $_REQUEST['object_id']);
            Tygh::$app['view']->display('addons/sd_order_files/views/sd_order_files/manage.tpl');
        }
        exit;
    }

    return array(CONTROLLER_STATUS_OK); // redirect should be performed via redirect_url always
}

if ($mode == 'getfile') {
    if (!empty($_REQUEST['attachment_id'])) {
        fn_get_sd_order_file($_REQUEST['attachment_id']);
    }

    exit;

} elseif ($mode == 'update') {
    // Assign attachments files for orders
    
    $sd_order_files = fn_get_sd_order_files($_REQUEST['object_type'], $_REQUEST['object_id'], 'M', DESCR_SL);

    Registry::set('navigation.tabs.sd_order_files', array (
        'title' => __('sd_order_files'),
        'js' => true
    ));
    
    Tygh::$app['view']->assign('sd_order_files', $sd_order_files);
}
