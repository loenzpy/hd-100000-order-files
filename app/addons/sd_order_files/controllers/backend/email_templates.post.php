<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Mailer\Message;
use Tygh\Mailer\MessageStyleFormatter;
use Tygh\Template\Mail\Template;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'update') {
    /** @var \Tygh\Template\Renderer $renderer */
    $renderer = Tygh::$app['template.renderer'];
	    /** @var \Tygh\Template\Mail\Repository $repository */
    $repository = Tygh::$app['template.mail.repository'];
    $view = Tygh::$app['view'];
    /** @var \Tygh\Template\Renderer $renderer */

 $email_template = $repository->findById($_REQUEST['template_id']);
     $variables = array_unique(array_merge(
        array('company_name', 'company_data', 'logos', 'styles', 'settings'),
        $renderer->retrieveVariables($email_template->getDefaultTemplate() . "\n" . $email_template->getDefaultSubject())
    ));

/*
* We add a new variable inserted into the notification template, which displays a list of order files in the letter through HOOK-function fn_sd_order_files_send_order_notification
*/

	$variables[] = 'order_info.files';

	$view->assign('variables', $variables);
}