<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }



if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    //
    // Create/update attachments
    //
    if ($mode == 'update') {

        $order_id_max = db_get_field("SELECT MAX(order_id) FROM ?:orders") + 1;

        fn_print_r($order_id_max);

        if (!empty($_REQUEST['attachment_data'])) {
            fn_update_sd_order_files($_REQUEST['attachment_data'], $_REQUEST['attachment_id'], 'order', $order_id_max, 'M', null, DESCR_SL);
        }

    }

    if ($mode == 'delete') {
        fn_delete_sd_order_files(array($_REQUEST['attachment_id']), $_REQUEST['object_type'], $_REQUEST['object_id']);
        $attachments = fn_get_sd_order_files($_REQUEST['object_type'], $_REQUEST['object_id']);
        if (empty($attachments)) {
            Tygh::$app['view']->assign('object_type', $_REQUEST['object_type']);
            Tygh::$app['view']->assign('object_id', $_REQUEST['object_id']);
            Tygh::$app['view']->display('addons/sd_order_files/views/sd_order_files/manage.tpl');
        }
        exit;
    }

    return array(CONTROLLER_STATUS_OK); // redirect should be performed via redirect_url always

}


if ($mode == 'getfile') {
    if (!empty($_REQUEST['attachment_id'])) {

        if (fn_get_sd_order_file($_REQUEST['attachment_id']) === false) {
            return array(CONTROLLER_STATUS_NO_PAGE);
        }

    }

    exit;
}

if ($mode == 'view') {

	$params = $_REQUEST;

    if (AREA == 'C') {
        $params['user_id'] = $auth['user_id'];
        fn_add_breadcrumb(__('files_in_orders'));
    }

    if (empty($params['user_id'])) {
        if (AREA == 'C') {
            return array(CONTROLLER_STATUS_REDIRECT, 'auth.login_form?return_url=' . urlencode(Registry::get('config.current_url')));
        } else {
            return array(CONTROLLER_STATUS_NO_PAGE);
        }
    }

	$sd_all_orders_files = fn_get_sd_all_orders_files();

	Tygh::$app['view']->assign('sd_all_orders_files', $sd_all_orders_files);
}

