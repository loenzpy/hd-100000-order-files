<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Storage;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'details') {

    $navigation_tabs = array(
        'general' => array(
            'title' => __('general'),
            'js' => false,
            'href' => 'orders.details?order_id=' . $_REQUEST['order_id'] . '&selected_section=general'
        ),
    );

    $file_name = db_get_field("SELECT filename FROM ?:sd_order_files WHERE object_id = ?i", $_REQUEST['order_id']);

    if (!empty($file_name)) {

    $navigation_tabs['files'] = array(
        'title' => __('files'),
        'js' => false,
        'href' => 'orders.details?order_id=' . $_REQUEST['order_id'] . '&selected_section=files'
    );
    
    }

    Registry::set('navigation.tabs', $navigation_tabs);

    if (!empty($_REQUEST['selected_section'])) {

        Tygh::$app['view']->assign('selected_section', $_REQUEST['selected_section']);

        $sd_all_orders_files = fn_get_sd_all_orders_files();
        
        Tygh::$app['view']->assign('sd_all_orders_files', $sd_all_orders_files);

    }


}
