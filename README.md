# README #

* Simtech Development: Order files add-on
* Developer name: Dmitriy Pirmatov
* Version: 1.0.0

### What is this repository for? ###

* Расширение позволяет к каждому заказу добавить неограниченное количество файлов (форматы не ограничены).
* Если у заказа есть файлы, то в письмах с информацией о заказе будут доступны ссылки на загрузку.
* Можно отфильтровать заказы по наличию файлов.
* На отдельной странице раздела “Заказы” будет доступна таблица с информацией “Заказ - Файл”.
* Покупатель может найти и скачать файлы в личном кабинете, в заказе и на отдельной странице.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact